package ru.mtumanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.ICommandRepository;
import ru.mtumanov.tm.api.service.ICommandService;
import ru.mtumanov.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    @NotNull
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @Override
    public void add(@Nullable final AbstractCommand abstractCommand) {
        if (abstractCommand == null)
            return;
        commandRepository.add(abstractCommand);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@NotNull final String name) {
        if (name.isEmpty())
            return null;
        return commandRepository.getCommandByName(name);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArgument(@NotNull final String arg) {
        if (arg.isEmpty())
            return null;
        return commandRepository.getCommandByArgument(arg);
    }

}
