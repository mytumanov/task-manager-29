package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.service.IProjectTaskService;
import ru.mtumanov.tm.api.service.ITaskService;
import ru.mtumanov.tm.command.AbstractCommand;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @NotNull
    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    protected void renderTask(final List<Task> tasks) {
        int index = 1;
        for (@Nullable final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task);
            index++;
        }
    }

    protected void showTask(@Nullable final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + task.getStatus());
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return Role.values();
    }

}
